<?php

namespace WPezThemeSukiChildForAU\App\Setup;

use WPezThemeSukiChildForAU\App\Core\HooksRegister\ClassHooksRegister;
use WPezThemeSukiChildForAU\App\Setup\Styles\ClassStyles;
use WPezThemeSukiChildForAU\App\Setup\Scripts\ClassScripts;
use WPezThemeSukiChildForAU\App\Setup\Menus\ClassMenus;


class ClassSetup {

	protected $arr_actions;

	protected $arr_filters;


	public function __construct() {

		$this->setPropertyDefaults();

		$this->scripts();

		$this->styles();

		$this->menus();

		// this must be last.
		$this->registerHooks();
	}

	protected function setPropertyDefaults() {

		$this->arr_actions = array();
		$this->arr_filters = array();
	}

	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function registerHooks() {

		$new_hooks_reg = new ClassHooksRegister();
		$new_hooks_reg->loadActions( $this->arr_actions );
		$new_hooks_reg->loadFilters( $this->arr_filters );
		$new_hooks_reg->doRegister();
	}


	protected function scripts( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// what
		$new_scripts = new ClassScripts();

		// add_action( 'wp_enqueue_scripts', [ $new_styles, 'wpEnqueueStyleParent' ] );
		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $new_scripts,
			'callback'  => 'wpEnqueueScriptChild',
		);

	}
	protected function styles( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// what
		$new_styles = new ClassStyles();

		// add_action( 'wp_enqueue_scripts', [ $new_styles, 'wpEnqueueStyleParent' ] );
		$this->arr_actions[] = array(
			'active'    => false, //   <<<
			'hook'      => 'wp_enqueue_scripts',
			'component' => $new_styles,
			'callback'  => 'wpEnqueueStyleParent',
		);

		// add_action( 'wp_enqueue_scripts', [ $new_styles, 'removeStyleParent' ] );
		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $new_styles,
			'callback'  => 'removeStyleParent',
			'priority'	=> PHP_INT_MAX,  // Run LAST!
		);		

		// add_action( 'wp_enqueue_scripts', [ $new_styles, 'wpEnqueueStyleChild' ], 10 );
		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $new_styles,
			'callback'  => 'wpEnqueueStyleChild',
		);

	}

	protected function menus( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// what.
		$new_menus = new ClassMenus();

		$arr = array(
			array(
				'loc'  => '404_menu',
				'desc' => '404 Menu',
			),
			array(
				'loc'  => 'menu_blog_categories',
				'desc' => 'Blog: Categories',
			),
			array(
				'loc'  => 'menu_blog_tags_tops',
				'desc' => 'Blog: Top Tags',
			),
			array(
				'loc'  => 'menu_blog_posts_trending',
				'desc' => 'Blog: Posts Trending',
			),
			array(
				'loc'  => 'menu_search_terms_tops',
				'desc' => 'Search: Top Terms',
			),
		);

		$new_menus->loadMenus( $arr );

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'after_setup_theme',
			'component' => $new_menus,
			'callback'  => 'register',
		);
	}
}
