<?php

namespace WPezThemeSukiChildForAU\App\Setup\Scripts;

class ClassScripts {

	protected $str_child_script     = 'main-child';
	protected $str_child_script_ver = 'v20221106.2:46pm';


	public function wpEnqueueScriptChild() {

		wp_enqueue_script(
			$this->str_child_script,
			get_stylesheet_directory_uri() . '/App/assets/dist/js/au.min.js',
			array(),
			$this->str_child_script_ver,
			true,
		);
	}
}
