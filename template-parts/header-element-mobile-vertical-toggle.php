<?php
/**
 * Mobile header vertical toggle template.
 *
 * Passed variables:
 *
 * @type string $element Header element.
 *
 * @package Suki
 */

// Prevent direct access.
if ( ! defined( 'ABSPATH' ) ) exit;

?>
<div class="<?php echo esc_attr( 'suki-header-' . $element ); ?>">
	<button class="suki-popup-toggle suki-toggle" data-target="mobile-vertical-header" aria-expanded="false">
		<span class="nav-hide-on-scroll">MENU</span>
		<?php suki_icon( 'menu', array( 'class' => 'suki-menu-icon' ) ); ?>
	</button>
</div>