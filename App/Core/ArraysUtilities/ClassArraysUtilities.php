<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 05-Mar-18
 * Time: 9:19 PM
 */

namespace WPezThemeSukiChildForAU\App\Core\ArraysUtilities;

class ClassArraysUtilities {


	/**
	 * Insert on array into another array.
	 *
	 * @param array   $arr_input Original array.
	 * @param array   $arr_insert Array to be inserted.
	 * @param integer $int_col Where the insert is maybe.
	 *
	 * @return array
	 */
	public static function insertArray( array $arr_input = array(), array $arr_insert = array(), $int_col = false ) {

		if ( false === $int_col ) {
			$int_col = count( $arr_input );
		}

		return array_merge(
			array_slice( (array) $arr_input, 0, $int_col, false ),
			(array) $arr_insert,
			array_slice( (array) $arr_input, $int_col, null, true )
		);

	}

	/**
	 * Insert on associative array into another associative array.
	 *
	 * @param array   $arr_input  Original array.
	 * @param array   $arr_insert Array to be inserted.
	 * @param integer $int_col Where the insert is maybe.
	 *
	 * @return array
	 */
	public static function insertArrayAssoc( array $arr_input = array(), array $arr_insert = array(), $int_col = false ) {

		if ( false === $int_col ) {
			$int_col = count( $arr_input );
		}

		return array_slice( (array) $arr_input, 0, $int_col, true )
		+ (array) $arr_insert
		+ array_slice( (array) $arr_input, $int_col, null, true );

	}

	/**
	 * TODO.
	 *
	 * @param [type] $args TODO.
	 *
	 * @return array
	 */
	public static function mergeDeepArray( $args ) {

		$arr_args = func_get_args();

		return self::mergeDeepPrep( $arr_args );
	}

	/**
	 * TODO.
	 *
	 * @param [type] $args
	 *
	 * @return array
	 */
	public static function mergeDeepObject( $args ) {

		$arr_args = func_get_args();
		$bool_obj = true;

		return self::mergeDeepPrep( $arr_args, $bool_obj );
	}

	/**
	 * TODO.
	 *
	 * @param [type] $args TODO.
	 *
	 * @return array
	 */
	public static function mergeDeepObjectPlus( $args ) {

		$arr_args  = func_get_args();
		$bool_obj  = true;
		$bool_plus = true;

		return self::mergeDeepPrep( $arr_args, $bool_obj, $bool_plus );
	}

	/**
	 * TODO
	 *
	 * @param array   $arr_args TODO.
	 * @param boolean $bool_obj TODO.
	 * @param boolean $bool_plus TODO.
	 *
	 * @return array
	 */
	protected static function mergeDeepPrep( array $arr_args = array(), bool $bool_obj = false, bool $bool_plus = false ) {

		$count   = count( $arr_args );
		$arr_ret = array();
		if ( 1 === $count ) {

			$plus    = array();
			$arr_ret = self::mergeDeepRecursive( $arr_args[0], $plus, $bool_obj, $bool_plus );

		} elseif ( $count > 1 ) {

			$arr_ret = self::mergeDeepRecursive( $arr_args[0], $arr_args[1], $bool_obj, $bool_plus );
			$x       = 2;

			while ( $x < $count ) {
				$bool_obj = false;
				if ( is_object( $arr_args[ $x ] ) ) {
					$bool_obj = true;
				}
				$arr_ret = self::mergeDeepRecursive( $arr_ret, $arr_args[ $x ], $bool_obj, $bool_plus );
				$x++;
			}
		}

		return $arr_ret;
	}


	/**
	 * As inspired by:
	 * https://mekshq.com/recursive-wp-parse-args-wordpress-function/
	 *
	 * @param array $base TODO.
	 * @param array $plus TODO.
	 * @param bool  $bool_obj TODO.
	 * @param bool  $bool_plus TODO.
	 *
	 * @return array|object
	 */
	protected static function mergeDeepRecursive( $base, &$plus, $bool_obj = false, $bool_plus = false ) {

		$bool_obj_it = false;

		if ( true === $bool_obj && is_object( $base ) ) {
			$bool_obj_it = true;
		} elseif ( true === $bool_plus && ( is_object( $base ) || is_object( $plus ) ) ) {
			$bool_obj_it = true;
		}

		$base   = (array) $base;
		$plus   = (array) $plus;
		$result = $base;
		foreach ( $plus as $k => &$v ) {
			if ( ( is_array( $v ) || is_object( $v ) ) && isset( $result[ $k ] ) ) {

				$result[ $k ] = self::mergeDeepRecursive( $result[ $k ], $v, $bool_obj, $bool_plus );
			} else {
				$result[ $k ] = $v;
			}
		}
		if ( $bool_obj_it ) {
			return (object) $result;
		}

		return $result;
	}
}
