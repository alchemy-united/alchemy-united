<?php
/**
 * Not found entry template.
 *
 * @package Suki
 */

// Prevent direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<section class="no-results not-found">
	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) { ?>

			<h1><?php esc_html_e( 'Nothing Found', 'suki' ); ?></h1>
			<p>
				<a href="<?php echo esc_url( admin_url( 'post-new.php' ) ); ?>"><?php esc_html_e( 'Ready to publish your first post? Get started here.', 'suki' ); ?></a>
			</p>

		<?php } elseif ( is_search() ) { ?>

			<p>
				<?php echo esc_html( __( 'Sorry, nothing matched your search term. Please try again.', 'suki' ) ); ?>
			</p>

		<?php } else { ?>

			<p>
				<?php echo esc_html( __( 'It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'suki' ) ); ?>
			</p>

			<?php get_search_form(); ?>

		<?php } ?>
	</div>
</section>
