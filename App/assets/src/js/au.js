document.addEventListener("DOMContentLoaded", function(){

    	let buttonOpen = document.querySelector('button.suki-popup-toggle.suki-toggle');
		let buttonClose = document.querySelector('button.suki-popup-close.suki-toggle');
		let eleBody = document.querySelector('body');

		if ( buttonOpen !== null && buttonClose !== null ){
	
			buttonOpen.addEventListener('click', navOpen, false);
			buttonOpen.addEventListener('touchend', navOpen, false);

			function navOpen(eve){
				eleBody.classList.remove('nav-open-false');
				eleBody.classList.add('nav-open-true');
			}

			buttonClose.addEventListener('click', navClose, false);
			buttonClose.addEventListener('touchend', navClose, false);

			function navClose(eve){
				eleBody.classList.remove('nav-open-true');
				eleBody.classList.add('nav-open-false');
			}
		}


    const openNewsletters = document.querySelectorAll('a[href^="#colophon"]');
    openNewsletters.forEach((openNewsletter, ndx) => {
        openNewsletter.addEventListener('click', function(e){
            e.preventDefault();
         //   openNewsletters[ndx].blur();
            openNewsletter.blur();
            if (document.getElementById('colophon') !== null){
                document.getElementById('colophon').scrollIntoView({behavior: "smooth"});
                const auNewsSignup = document.querySelector('#au-newsletter-signup');
                if (auNewsSignup !== null){
                    auNewsSignup.open = true;
                }
            }
        });

    });

    const comments = document.querySelector('.au-entry-meta .comments-link a');

    if ( comments !== null ) {
        comments.addEventListener('click', function(event){
            event.preventDefault();
            const detailsComments = document.getElementById('details-comments');
            detailsComments.open = true;
            document.getElementById('details-comments').scrollIntoView({behavior: "smooth"});
        });
    } 

    const auB2T = document.querySelectorAll('.au-b2t');
    if (auB2T !== null ) {
        auB2T.forEach( function( ele, ndx ) {
            ele.addEventListener("click", function(event) {
                ele.blur();
                event.preventDefault();
                window.scrollTo({
                    top: 0,
                    behavior: 'smooth'
                });
            });
        });
    }

    var winH = window.innerHeight;
    var statusScroll = false;
    var statusScroll40 = false;

    function classToggle( posY ){
        var scrollClass = "has-scrolled";
        var scrollClass40 = "has-scrolled40";
        if ( posY / winH > 0.001 ){
            if ( false === statusScroll ){
                statusScroll = true;
                document.body.classList.add(scrollClass);
            }
        } else {
            statusScroll = false;
            document.body.classList.remove(scrollClass);
        }
        if ( posY / winH > 0.2 ){
            if ( false === statusScroll40 ){
                statusScroll40 = true;
                document.body.classList.add(scrollClass40);
            }
        } else {
            statusScroll40 = false;
            document.body.classList.remove(scrollClass40);
        }
    }
    
    var scrollY = window.scrollY;
    classToggle( scrollY );
    document.onscroll = function(){
        scrollY = window.scrollY;
        classToggle( scrollY );
    };


    // ref: https://css-tricks.com/how-to-animate-the-details-element-using-waapi/
    class AnimateDetails {

        constructor(el) {
            // Store the <details> element
            this.el = el;
            // Store the <summary> element
            this.summary = el.querySelector('summary');
            // Store the <div class="details-inner"> element
            this.details_inner = el.querySelector('.details-inner');

            // Store the animation object (so we can cancel it if needed)
            this.animation = null;
            // Store if the element is closing
            this.isClosing = false;
            // Store if the element is expanding
            this.isExpanding = false;
            // Detect user clicks on the summary element
            this.summary.addEventListener('click', (e) => this.onClick(e));
        }

        onClick(e) {
            // Stop default behaviour from the browser
            e.preventDefault();
            // Add an overflow on the <details> to avoid content overflowing
            this.el.style.overflow = 'hidden';
            // Check if the element is being closed or is already closed
            if (this.isClosing || !this.el.open) {
            this.open();
            // Check if the element is being openned or is already open
            } else if (this.isExpanding || this.el.open) {
            this.shrink();
            }
        }

        shrink() {
            // Set the element as "being closed"
            this.isClosing = true;

            // Store the current height of the element
            const startHeight = `${this.el.offsetHeight}px`;
            // Calculate the height of the summary
            const endHeight = `${this.summary.offsetHeight}px`;

            // If there is already an animation running
            if (this.animation) {
            // Cancel the current animation
            this.animation.cancel();
            }

            // Start a WAAPI animation
            this.animation = this.el.animate({
            // Set the keyframes from the startHeight to endHeight
            height: [startHeight, endHeight]
            }, {
            duration: 400,
            easing: 'ease-out'
            });

            // When the animation is complete, call onAnimationFinish()
            this.animation.onfinish = () => this.onAnimationFinish(false);
            // If the animation is cancelled, isClosing variable is set to false
            this.animation.oncancel = () => this.isClosing = false;
        }

        open() {
            // Apply a fixed height on the element
            this.el.style.height = `${this.el.offsetHeight}px`;
            // Force the [open] attribute on the details element
            this.el.open = true;
            // Wait for the next frame to call the expand function
            window.requestAnimationFrame(() => this.expand());
        }

        expand() {
            // Set the element as "being expanding"
            this.isExpanding = true;
            // Get the current fixed height of the element
            const startHeight = `${this.el.offsetHeight}px`;
            // Calculate the open height of the element (summary height + content height)
            const endHeight = `${this.summary.offsetHeight + this.details_inner.offsetHeight}px`;

            // If there is already an animation running
            if (this.animation) {
            // Cancel the current animation
            this.animation.cancel();
            }

            // Start a WAAPI animation
            this.animation = this.el.animate({
            // Set the keyframes from the startHeight to endHeight
            height: [startHeight, endHeight]
            }, {
            duration: 400,
            easing: 'ease-out'
            });
            // When the animation is complete, call onAnimationFinish()
            this.animation.onfinish = () => this.onAnimationFinish(true);
            // If the animation is cancelled, isExpanding variable is set to false
            this.animation.oncancel = () => this.isExpanding = false;
        }

        onAnimationFinish(open) {
            // Set the open attribute based on the parameter
            this.el.open = open;
            // Clear the stored animation
            this.animation = null;
            // Reset isClosing & isExpanding
            this.isClosing = false;
            this.isExpanding = false;
            // Remove the overflow hidden and the fixed height
            this.el.style.height = this.el.style.overflow = '';
        }
    };

    document.querySelectorAll("details").forEach((el) => {
    new AnimateDetails(el);
    });	
});