<?php
/**
 * As inspired by:
 *
 * Https://code.tutsplus.com/tutorials/object-oriented-programming-in-wordpress-building-the-plugin-ii--cms-21105
 *
 * https://github.com/DevinVinson/WordPress-Plugin-Boilerplate/blob/master/plugin-name/includes/class-plugin-name-loader.php
 *
 * The notable ez improvement is loading is done with arrays, not (harcoded) code.
 * This means, you can define your hooks and then - prior to loading -
 * slap a filter on those arrays to allow for customization by users (read:
 * other devs). As a result any hook'ed callback (method) can be replaced as
 * needed.
 *
 * Yeah. Cool :)
 *
 * @package WPezThemeSukiChildForAU\App\Core\HooksRegister
 */

namespace WPezThemeSukiChildForAU\App\Core\HooksRegister;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassHooksRegister {

	/**
	 * Array of actions to be registered.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Array of filters to be registered.
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Defaults for the hooks being registered.
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;


	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();

	}

	/**
	 * Set the defaults for the properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		// $this->arr_types = ['action', 'filter'];

		$this->arr_actions = array();
		$this->arr_filters = array();

		$this->arr_hook_defaults = array(
			'active'        => true,
			'hook'          => false,
			'component'     => false,
			'callback'      => false,
			'priority'      => 10,
			'accepted_args' => 1,
		);
	}

	/**
	 * Push a single action onto the actions' array.
	 *
	 * @param array $arr_args Array of action's args.
	 *
	 * @return bool
	 */
	public function pushAction( array $arr_args ) {

		return $this->pushMaster( 'arr_actions', $arr_args );

	}

	/**
	 * Push a single filter onto the filters' array.
	 *
	 * @param array $arr_args Array of filter's args.
	 *
	 * @return bool
	 */
	public function pushFilter( $arr_args = false ) {

		return $this->pushMaster( 'arr_filters', $arr_args );

	}


	/**
	 * Ultimately, the method that does the pushing
	 *
	 * @param boolean $str_prop The hook type, arr_filters or arr_actions.
	 * @param boolean $arr_args The hooks' args to be pushed.
	 *
	 * @return bool
	 */
	protected function pushMaster( $str_prop = false, $arr_args = false ) {

		if ( ! is_array( $arr_args ) || ! is_string( $str_prop ) ) {
			return false;
		}

		$obj_hook = (object) array_merge( $this->arr_hook_defaults, $arr_args );

		// maybe we have a problem?
		if ( true !== $obj_hook->active || false === $obj_hook->hook || false === $obj_hook->component || false === $obj_hook->callback ) {
			return false;
		}

		$this->$str_prop[] = $obj_hook;
		return true;

	}

	/**
	 * Load actions in bulk (not one at a time).
	 *
	 * @param array $arr_arrs Array of actions.
	 *
	 * @return bool
	 */
	public function loadActions( $arr_arrs = array() ) {

		if ( ! is_array( $arr_arrs ) ) {
			return false;
		}

		$this->loadMaster( 'pushAction', $arr_arrs );
		return true;

	}

	/**
	 * Load filters in bulk (not one at a time).
	 *
	 * @param array $arr_arrs Array of filters.
	 *
	 * @return bool
	 */
	public function loadFilters( $arr_arrs = array() ) {

		if ( ! is_array( $arr_arrs ) ) {
			return false;
		}
		$this->loadMaster( 'pushFilter', $arr_arrs );
		return true;
	}


	/**
	 * Ultimately, this method does the loading.
	 *
	 * @param string $str_method push what? 'pushAction' or 'pushFilter'.
	 * @param array $arr_arrs The filters' args to be pushed.
	 *
	 * @return void
	 */
	protected function loadMaster( string $str_method, $arr_arrs = array() ) {

		foreach ( $arr_arrs as $arr ) {

			$this->$str_method( $arr );

		}
	}


	/**
	 * Register some hook type(s).
	 *
	 * @param boolean $bool_actions Registering actions?.
	 * @param boolean $bool_filters Registering filters?.
	 *
	 * @return void
	 */
	public function doRegister( $bool_actions = true, $bool_filters = true ) {

		if ( true === $bool_actions && ! empty( $this->arr_actions ) ) {

			$this->register( 'add_action', $this->arr_actions );
		}

		if ( true === $bool_filters && ! empty( $this->arr_filters ) ) {

			$this->register( 'add_filter', $this->arr_filters );
		}

	}

	/**
	 * Ultimately, this method does the doRegister().
	 *
	 * @param string $str_wp_function Which WP hooks function? 'add_action' or 'add_filter'.
	 * @param array $arr_objs The array of hooks to be registered
	 *
	 * @return void
	 */
	protected function register( $str_wp_function = '', $arr_objs = array() ) {

		foreach ( $arr_objs as $obj ) {

			$str_wp_function( $obj->hook, array( $obj->component, $obj->callback ), $obj->priority, $obj->accepted_args );
		}
	}

}
