<?php

namespace WPezThemeSukiChildForAU\App\Setup\Styles;

class ClassStyles {

	/**
	 * The name of the parent's enqueue'd style.
	 *
	 * @var string
	 */
	protected $str_parent_style = 'suki';

	/**
	 * Parent theme's version. Be sure to update if / when the parent is updated.
	 *
	 * @var string
	 */
	protected $str_parent_style_ver = 'v1.3.7';

	/**
	 * The name of this child's enqueue'd style.
	 *
	 * @var string
	 */
	protected $str_child_style      = 'main-child';

	/**
	 * Parent child's version. Be sure to update if / when the CSS is updated.
	 *
	 * @var string
	 */
	protected $str_child_style_ver  = 'v20221106.3:17pm';

	public function __construct() {}

	/**
	 * Remove the parent css, as well as the inline customizer CSS.
	 *
	 * @return void
	 */
	public function removeStyleParent() {

		if ( isset( wp_styles()->registered['suki']->extra['after'][0] ) ) {
			wp_styles()->registered['suki']->extra['after'][0] = '';
			unset( wp_styles()->registered['suki'] );
		}

	}

	/**
	 * Emqueue the parent's style.
	 *
	 * @return void
	 */
	public function wpEnqueueStyleParent() {

		wp_enqueue_style(
			$this->str_parent_style . '-primary',
			get_template_directory_uri() . '/style.css',
			array(),
			$this->str_parent_style_ver
		);

	}


	/**
	 * Enqueue the child style.
	 *
	 * @return void
	 */
	public function wpEnqueueStyleChild() {

		wp_enqueue_style(
			$this->str_child_style,
			get_stylesheet_directory_uri() . '/App/assets/dist/css/main.min.css',
			array(),
			$this->str_child_style_ver
		);
	}
}
