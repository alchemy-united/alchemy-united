<?php

namespace WPezThemeSukiChildForAU;

if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassAutoload {

	/**
	 * "Root" of the namespace being AL'ed.
	 *
	 * @var string
	 */
	protected $str_needle_root;

	/**
	 * The child of the root.
	 *
	 * @var string
	 */
	protected $str_needle_child;

	/**
	 * Base path of where the AL'ed class is located.
	 *
	 * @var string
	 */
	protected $str_path_base;

	/**
	 * Remove this string to build the correct path.
	 *
	 * @var string
	 */
	protected $str_remove_from_class;

	 /**
	  * Class constructor.
	  */
	public function __construct() {

		$this->setPropertyDefaults();

		spl_autoload_register( null, false );

	}

	/**
	 * Sets the properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->str_needle_root       = __NAMESPACE__;
		$this->str_needle_child      = false;
		$this->str_path_base         = false;
		$this->str_remove_from_class = __NAMESPACE__;
	}

	/**
	 * Setter for needle root.
	 *
	 * @param string $str The value to set.
	 *
	 * @return void
	 */
	public function setNeedleRoot( string $str ) {

		$this->str_needle_root       = $str;
		$this->str_remove_from_class = $str;
	}

	/**
	 * Setter for needle child.
	 *
	 * @param string $str The value to set.
	 *
	 * @return void
	 */
	public function setNeedleChild( string $str ) {

		$this->str_needle_child = $str;
	}

	/**
	 * Setter for path base.
	 *
	 * @param string $str The value to set.
	 *
	 * @return void
	 */
	public function setPathBase( string $str ) {

		$this->str_path_base = $str;
	}

	/**
	 * Setter for remove from class.
	 *
	 * @param string $str The value to set.
	 *
	 * @return void
	 */
	public function setRemoveFromClass( string $str ) {

		$this->str_remove_from_class = $str;
	}


	/**
	 * (@link http://www.phpro.org/tutorials/SPL-Autoload.html)
	 *
	 * The classes naming convention allows us to parse the folder structure
	 * out of the class / file name. And then use that to define the $file for
	 * the require_once()
	 *
	 * @param string $str_class The class as passed in by PHP.
	 *
	 * @return bool
	 */
	public function wpezAutoload( $str_class ) {

		if ( strrpos( $str_class, $this->str_needle_root, -strlen( $str_class ) ) === false ) {

			return false;
		}

		$arr_class = explode( '\\', $str_class );

		if ( count( $arr_class ) < 3 || false === $this->str_needle_child || ! isset( $arr_class[1] ) || $arr_class[1] !== $this->str_needle_child ) {

			return false;
		}

		$str_file = implode( DIRECTORY_SEPARATOR, $arr_class );
		$str_file = ltrim( str_replace( $this->str_remove_from_class, '', $str_file ), DIRECTORY_SEPARATOR );
		$str_file = rtrim( $this->str_path_base, '/' ) . DIRECTORY_SEPARATOR . $str_file . '.php';

		if ( file_exists( $str_file ) ) {

			require $str_file;

			return true;
		}

		return false;
	}

}
