<?php
/**
 * For registering menus.
 *
 * @package WPezThemeSukiChildForAU\App\Setup\Menus
 */

namespace WPezThemeSukiChildForAU\App\Setup\Menus;

/**
 * Assorted helper methods to make registering menus ez.
 */
class ClassMenus {

	/**
	 * Defaults for a given menu's array of args.
	 *
	 * @var array
	 */
	protected $arr_menu_defaults;

	/**
	 * Array of menus to be registered.
	 *
	 * @var array
	 */
	protected $arr_menus;


	/**
	 * The class' constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Sets the defaults for the various properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_menu_defaults = array(
			'active' => true,
			'loc' => false,
			'desc' => false,
		);
		$this->arr_menus = array();
	}

	/**
	 * Load the menus to be registered.
	 *
	 * @param boolean $arr Array of menus.
	 *
	 * @return void
	 */
	public function loadMenus( $arr = false ) {

		if ( is_array( $arr ) ) {
			$this->arr_menus = $arr;
		}

	}

	/**
	 * Registers the menus in the array of menus.
	 *
	 * @return void
	 */
	public function register() {

		foreach ( $this->arr_menus as $arr_menu ) {

			if ( ! is_array( $arr_menu ) ) {
				continue;
			}

			$arr_reg = array_merge( $this->arr_menu_defaults, $arr_menu );

			if ( ! is_string( $arr_reg['loc'] ) || ! is_string( $arr_reg['desc'] ) || true !== $arr_reg['active'] ) {
				continue;
			}

			register_nav_menu( $arr_reg['loc'], $arr_reg['desc'] );
		}
	}
}
