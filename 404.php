<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Suki
 */

// Prevent direct access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Header
 */
get_header();

/**
 * Primary - opening tag
 */
suki_primary_open();

/**
 * Hook: suki/frontend/before_main
 */
do_action( 'suki/frontend/before_main' );

echo '<div class="hero-wrapper">';
echo '<div class="entry-header suki-text-align-center">';
echo '<h1 class="entry-title">';
esc_html_e( 'Drats. Page not found.', 'suki' );
echo '</h1>';
if ( has_nav_menu( '404_menu' ) ) {
	echo '<h2 class="suggested-404">';
	echo esc_html( 'Could it be one of these?', 'suki' );
	echo '</h2>';
	wp_nav_menu(
		array(
			'container'            => 'nav',
			'container_class'      => 'nav-404-suggestions',
			'container_aria_label' => 'nav for page not found suggestions',
			'theme_location'       => '404_menu',
			'menu_class'           => 'menu',
			'depth'                => -1,
		)
	);
}
?>
<h2 class="search-404">
<?php
echo esc_html( 'Or to search, scroll down a bit.', 'suki' );
?>
</h2>
<?php
echo '</div>';
echo '</div>';

/**
 * Hook: suki/frontend/after_main
 */
do_action( 'suki/frontend/after_main' );

/**
 * Primary - closing tag
 */
suki_primary_close();

/**
 * Sidebar
 */
get_sidebar();

/**
 * Footer
 */
get_footer();
